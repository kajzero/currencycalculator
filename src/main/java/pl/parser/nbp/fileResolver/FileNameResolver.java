package pl.parser.nbp.fileResolver;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FileNameResolver {
    public static final String DATE_FOR_FILE_FORMAT = "yyMMdd";
    private LocalDate fromDate;
    private LocalDate toDate;

    public FileNameResolver(String fromDate, String toDate) {
        this.fromDate = LocalDate.parse(fromDate);
        this.toDate = LocalDate.parse(toDate);
    }


    public List<String> generateAllPossibleFileEndingsToAnalize() {
        List<String> possibleFileEndings = new ArrayList();
        DateTimeFormatter fmt = DateTimeFormat.forPattern(DATE_FOR_FILE_FORMAT);
        for (LocalDate date = fromDate; date.isBefore(toDate) || date.isEqual(toDate); date = date.plusDays(1)) {
            possibleFileEndings.add(date.toString(fmt));
        }

        return possibleFileEndings;
    }

    public List<String> getListOfRequiredFilesWithList() {
        List<String> listOfRequiredFilesWithList = new ArrayList<>();
        for (LocalDate date = fromDate; date.isBefore(toDate); date = date.plusYears(1)) {
            if (date.getYear() == LocalDate.now().getYear()) {
                listOfRequiredFilesWithList.add("dir.txt");
            } else {
                listOfRequiredFilesWithList.add("dir" + date.getYear() + ".txt");
            }
        }
        return listOfRequiredFilesWithList;
    }

    public List<String> resolveBetweenSmartListAndReceivedList(List<String> smartList, List<String> receivedList){
        return receivedList.stream().filter(s -> smartList.contains(s.substring(5, 11))).collect(Collectors.toList());

    }

}
