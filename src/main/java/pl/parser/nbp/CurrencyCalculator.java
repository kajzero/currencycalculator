package pl.parser.nbp;

import org.xml.sax.SAXException;
import pl.parser.nbp.client.NBPClient;
import pl.parser.nbp.fileResolver.FileNameResolver;
import pl.parser.nbp.math.Calculator;
import pl.parser.nbp.model.SellAndBuyRate;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Created by mateuszkaszyk on 22.03.2017.
 */
public class CurrencyCalculator {
    private NBPClient nbpClient = new NBPClient();
    private List<String> requiredTableOfContent;
    private List<SellAndBuyRate> prices;
    private List<String> finalXmlFileList;


    public void execute(String currencyCode, String fromDate, String tillDate) {
        Locale.setDefault(new Locale("en", "US"));
        FileNameResolver fileNameResolver = new FileNameResolver(fromDate, tillDate);
        List<String> fileNamesWithTableOfContent = fileNameResolver.getListOfRequiredFilesWithList();
        List<String> possibleEndings = fileNameResolver.generateAllPossibleFileEndingsToAnalize();

        try {
            requiredTableOfContent = nbpClient.getRequiredFileList(fileNamesWithTableOfContent);
            finalXmlFileList = fileNameResolver.resolveBetweenSmartListAndReceivedList(possibleEndings,requiredTableOfContent);
            prices = nbpClient.getSellAndBuyPricesFromFiles(finalXmlFileList, currencyCode);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        System.out.printf("%.4f\n",Calculator.average(prices.stream().map(SellAndBuyRate::getBuyRate).collect(Collectors.toList())));
        System.out.printf("%.4f",Calculator.standardDeviation(prices.stream().map(SellAndBuyRate::getSellRate).collect(Collectors.toList())));


    }

}
