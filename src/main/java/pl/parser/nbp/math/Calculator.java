package pl.parser.nbp.math;

import java.util.List;

/**
 * Created by mateuszkaszyk on 22.03.2017.
 */
public class Calculator {
    public static double average(List<Double> numbers) {
        return numbers.stream().mapToDouble(d -> d).average().orElse(0);
    }

    public static double standardDeviation(List<Double> numbers) {
      return Math.sqrt(variance(numbers));
    }

    private static double variance(List<Double> numbers){
        double mean = average(numbers);
        return numbers.stream().map(d -> {d = Math.pow(mean - d, 2);return d;}).mapToDouble(d -> d).sum() / (numbers.size());

   //TODO testy
    }

}
