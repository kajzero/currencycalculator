package pl.parser.nbp.xpath;

/**
 * Created by mateuszkaszyk on 22.03.2017.
 */
public class XPathBuilder {
    public static String BASE_XPATH = "tabela_kursow/pozycja[kod_waluty='";
    public static String SELL_NODE = "/kurs_sprzedazy";
    public static String BUY_NODE = "/kurs_kupna";

    public static String getXPathToSellRate(String currency){
        return BASE_XPATH +currency + "']"+ SELL_NODE;

    }
    public static String getXPathToBuyRate(String currency){
        return BASE_XPATH + currency +"']"+ BUY_NODE;
    }
}
