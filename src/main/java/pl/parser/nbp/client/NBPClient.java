package pl.parser.nbp.client;

import com.google.common.annotations.VisibleForTesting;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import pl.parser.nbp.model.SellAndBuyRate;
import pl.parser.nbp.parser.Parser;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mateuszkaszyk on 20.03.2017.
 */
public class NBPClient {
    public static final String BASE_URL_TO_NBP = "http://www.nbp.pl/kursy/xml/";
    Parser parser = new Parser();

    public List<String> getRequiredFileList(List<String> filesToOpen) throws IOException {
        boolean isFirstLine = true;
        List<String> fileNamesWithData = new ArrayList<>();
        for(String fileToOpen: filesToOpen){
            BufferedReader in = getStream(fileToOpen);
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                    if(isFirstLine){
                        inputLine=inputLine.substring(1);
                        isFirstLine=false;
                    }
                    if(inputLine.startsWith("c")) {
                        fileNamesWithData.add(inputLine);
                    }
                }
            in.close();
        }
        return fileNamesWithData;
    }

    public List<SellAndBuyRate> getSellAndBuyPricesFromFiles(List<String> fileNames, String currencyCode) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        List<SellAndBuyRate> sellAndBuyRates = new ArrayList<>();
        for(String fileName : fileNames) {
            Document doc = prepareDoc(fileName,db);
            sellAndBuyRates.addAll(parser.extractSellAndBuyRate(doc,currencyCode));

        }
        return sellAndBuyRates;
    }

    @VisibleForTesting
    public Document prepareDoc(String fileName, DocumentBuilder db) throws IOException, SAXException, ParserConfigurationException {
        Document doc = db.parse(BASE_URL_TO_NBP + fileName + ".xml");
        doc.getDocumentElement().normalize();
        return doc;
    }
    private BufferedReader getStream(String fileToOpen){
        URL url;
        BufferedReader in = null;
        try {
            url = new URL(BASE_URL_TO_NBP + fileToOpen);
            in = new BufferedReader(new InputStreamReader(url.openStream()));

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return in;
    }

}
