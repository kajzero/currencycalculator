package pl.parser.nbp.model;

/**
 * Created by mateuszkaszyk on 22.03.2017.
 */
public class SellAndBuyRate {
    private double sellRate;
    private double buyRate;

    public SellAndBuyRate(double sellRate, double buyRate){
        this.buyRate=buyRate;
        this.sellRate=sellRate;
    }

    public double getBuyRate() {
        return buyRate;
    }

    public double getSellRate() {
        return sellRate;
    }
}
