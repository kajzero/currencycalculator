package pl.parser.nbp.parser;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import pl.parser.nbp.model.SellAndBuyRate;
import pl.parser.nbp.xpath.XPathBuilder;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by mateuszkaszyk on 22.03.2017.
 */
public class Parser {
    XPath xPath = XPathFactory.newInstance().newXPath();

    public List<SellAndBuyRate> extractSellAndBuyRate(Document document, String currency) {
        List<SellAndBuyRate> ratesList = new ArrayList<>();
        try {
            Node buyNode = (Node) xPath.compile(XPathBuilder.getXPathToBuyRate(currency)).evaluate(document, XPathConstants.NODE);
            Node sellNode = (Node) xPath.compile(XPathBuilder.getXPathToSellRate(currency)).evaluate(document, XPathConstants.NODE);
            ratesList.add(nodesToSellandBuyValue(sellNode, buyNode));
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return ratesList;
    }

    private SellAndBuyRate nodesToSellandBuyValue(Node sellNode, Node buyNode) {
        NumberFormat comaFormat = NumberFormat.getInstance(Locale.GERMAN);
        double sellRate = 0, buyRate=0;
        try {
            sellRate = comaFormat.parse(sellNode.getTextContent()).doubleValue();
            buyRate = comaFormat.parse(buyNode.getTextContent()).doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SellAndBuyRate(sellRate, buyRate);
    }


}
