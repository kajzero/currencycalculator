package pl.parser.nbp.xpath;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by mateuszkaszyk on 24.03.2017.
 */
public class XPathBuilderTest {

    @Test
    public void testGetXPathToSellRate() throws Exception {
        //given
        String currency = "USD";
        //when
        String xPath = XPathBuilder.getXPathToSellRate(currency);
        //then
        assertEquals("tabela_kursow/pozycja[kod_waluty='USD']/kurs_sprzedazy", xPath);
    }

    @Test
    public void testGetXPathToBuyRate() throws Exception {
        //given
        String currency = "EUR";
        //when
        String xPath = XPathBuilder.getXPathToBuyRate(currency);
        //then
        assertEquals("tabela_kursow/pozycja[kod_waluty='EUR']/kurs_kupna", xPath);

    }
}