package pl.parser.nbp.fileResolver;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by mateuszkaszyk on 20.03.2017.
 */
public class FileNameResolverTest {
    private FileNameResolver fileNameResolver;
    @Before
    public void setUp(){
        fileNameResolver = new FileNameResolver("2012-12-21", "2012-12-23");
    }
    @Test
    public void should_generate_all_possible_xml_file_endings_for_provided_dates(){
        //given setUp
        //when
        List<String> fileEndings = fileNameResolver.generateAllPossibleFileEndingsToAnalize();
        //then
        assertEquals(Arrays.asList("121221","121222","121223"),fileEndings);
    }

}