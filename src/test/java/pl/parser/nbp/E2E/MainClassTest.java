package pl.parser.nbp.E2E;

import org.junit.Test;
import pl.parser.nbp.MainClass;

import java.io.ByteArrayOutputStream;

import static junit.framework.TestCase.assertEquals;

public class MainClassTest extends CurrencyCalculatorE2EBase {
    @Test
    public void should_solve_problem_as_provided_in_example() {
        //given
        String[] arguments = $("EUR", "2013-01-28", "2013-01-31");
        ByteArrayOutputStream systemOutPrintlnStream = redirectOutputStream();

        //when
        MainClass.main(arguments);

        //then
        assertEquals("4.1505\n" +
                "0.0125", systemOutPrintlnStream.toString());
    }


}
