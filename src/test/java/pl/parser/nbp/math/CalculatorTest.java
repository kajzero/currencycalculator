package pl.parser.nbp.math;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;


/**
 * Created by mateuszkaszyk on 24.03.2017.
 */
public class CalculatorTest {

    @Test
    public void testAverage() throws Exception {
        //given
        List<Double> numbers = Arrays.asList(5.0,4.0,12.0,2.0,1.0,6.0);
        //when
        double result = Calculator.average(numbers);
        //then
        assertEquals(5.0,result);
    }

    @Test
    public void testStandardDeviation() throws Exception {
        //given
        List<Double> numbers = Arrays.asList(5.0,5.0,3.0,4.0,3.0,3.0,4.0);
        //when
        double result = Calculator.standardDeviation(numbers);
        //then
        assertEquals(0.832993127835043,result);
    }
}